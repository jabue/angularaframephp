app.controller("editCtrl", function($scope, $http, Upload, $rootScope, $location, appConfig, projectService) {

    $scope.clickTimeStamp = 0;
    // current edit target
    $scope.currentElement = null;
    // user set target, choosed scene
    $scope.selectedScene = null;
    // user set scene background
    $scope.selectedBgImageEl = null;
    // user set scene background img src
    $scope.selectedBgImageSrc = null;

    // deselect target
    $scope.deselectTarget = function($event) {
        if (!$scope.isMouseDown && $scope.clickTimeStamp < $event.timeStamp) {
            if ($scope.currentElement) {
                $scope.currentElement.setAttribute('color', 'white');
                $('#camera').attr('draggable-component', false);
                // set hotspot new position
                var hotspotName = $scope.currentElement.getAttribute("id");
                $scope.sceneData.hotspots.forEach(function(hotspot) {
                    if (hotspot.name == hotspotName) {
                        var positionData = $scope.currentElement.getAttribute('position');
                        hotspot.position = positionData.x + ' ' + positionData.y + ' ' + positionData.z;
                    }
                });
                $scope.currentElement = null;
                projectService.saveToServer();
            }
            closeHotspotPropertyPanel();
            closeAddElementPanel();
        } else {
            $scope.isMouseDown = false;
        }
    }

    // select target to edit
    $scope.selectTarget = function($event) {
        $scope.clickTimeStamp = $event.timeStamp;
        if ($scope.currentElement) {
            $scope.currentElement.setAttribute('color', 'white');
        }
        $scope.currentElement = $event.currentTarget;
        $scope.currentElement.setAttribute('color', 'red');
        $('#currentHotspot').attr('data', $event.currentTarget.getAttribute('id'));
        $('#camera').attr('draggable-component', true);
        openHotspotPropertyPanel();
        $scope.isMouseDown = false;
    }

    $scope.checkMouseDown = function() {
        $scope.isMouseDown = true;
    }

    $scope.openScene = function(sceneName) {
        $scope.sceneData = projectService.getSceneData(sceneName);
    };

    $scope.switchScene = function(sceneName) {
        $scope.sceneData = projectService.getSceneData(sceneName);
        changeSky();
    }

    $scope.submitHotSpot = function($event) {
        $('.fixed-action-btn.toolbar').closeToolbar();
    };

    $scope.addHotspot = function() {
        var point = generateCenterPosition();
        var pos = point.x + " " + point.y + " " + point.z;
        $scope.sceneData.hotspots.push({
            "name": projectService.generateRandomString(),
            "position": pos,
            "target": " "
        });
        closeAddElementPanel();
        projectService.saveToServer();
    };

    $scope.removeHotspot = function() {
        var hotspotName = $scope.currentElement.getAttribute("id");
        for (var i = 0; i < $scope.sceneData.hotspots.length; i++) {
            if ($scope.sceneData.hotspots[i].name === hotspotName) {
                $scope.sceneData.hotspots.splice(i, 1);
                break;
            }
        }
        $('#camera').attr('draggable-component', false);
        closeHotspotPropertyPanel();
        projectService.saveToServer();
    }

    $scope.openAddElementPanel = function() {
        // $('#firstLevelPanel').addClass("in");
        $('#collapseExample2').removeClass("in");
        $('#collapseExample3').removeClass("in");
        $('#collapseExample').addClass("in");
    }

    $scope.openSceneList = function() {
        openSceneModal();
    }

    $scope.closeSceneList = function() {
        closeSceneModal();
    }

    $scope.submitSceneTarget = function() {
        // set hotspot target
        var hotspotName = $scope.currentElement.getAttribute("id");
        var targetSceneName = $scope.selectedScene.getAttribute("id");
        var currentSceneName = $scope.sceneData.name;
        $scope.sceneData.hotspots.forEach(function(hotspot) {
            if (hotspot.name == hotspotName) {
                hotspot.target = targetSceneName;
            }
        });
        closeSceneModal();
        projectService.saveToServer();
    }

    $scope.selectScene = function($event) {
        if ($scope.selectedScene) {
            $scope.selectedScene.classList.remove("choosed");
        }
        $scope.selectedScene = $event.currentTarget;
        $scope.selectedScene.classList.add("choosed");
    }

    $scope.openBgList = function() {
        openBgModal();
    }

    $scope.closeBgList = function() {
        closeBgModal();
        if ($scope.selectedBgImageEl) {
            $scope.selectedBgImageEl.classList.remove("choosed");
            $scope.selectedBgImageEl = null;
        }
        $scope.selectedBgImageSrc = null;
    }

    $scope.selectBgImage = function($event, image) {
        if ($scope.selectedBgImageEl) {
            $scope.selectedBgImageEl.classList.remove("choosed");
        }
        $scope.selectedBgImageEl = $event.currentTarget;
        $scope.selectedBgImageEl.classList.add("choosed");
        $scope.selectedBgImageSrc = image.replace("previews", "result");;
    }

    $scope.submitBgImage = function() {
        $scope.sceneData.src = $scope.selectedBgImageSrc;
        projectService.saveToServer();
        changeSky();
        closeBgModal();
    }

    $scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: appConfig.backend + '/vrproject/uploadImage',
                data: { file: file }
            });

            file.upload.then(function(response) {
                console.log(response.data);
                $scope.sceneData.src = appConfig.backend + "/vrimages/" + response.data.path;
                changeSky();
                projectService.saveToServer();
            }, function(response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function(evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
            });
        }
    }

    $scope.addScene = function() {
        var sceneName = projectService.generateRandomString();
        $scope.scenesList.push({
            "name": sceneName,
            "src": "./image/sky.jpg",
            "default": false,
            "hotspots": []
        });
        projectService.saveToServer();
        $scope.sceneData = projectService.getSceneData(sceneName);
    }

    $scope.deleteCurScene = function() {
        if ($scope.scenesList.length == 1) {
            alert('Sorry, there is only one scene left, you cannot delete.');
        } else {
            $scope.scenesList.forEach(function(scene, index) {
                if (scene.name == $scope.sceneData.name) {
                    $scope.scenesList.splice(index, 1);
                }
            });
            $scope.sceneData = $scope.scenesList[0];
            changeSky();
            projectService.saveToServer();
        }
    }

    $scope.saveWholeProject = function() {
        projectService.saveToServer();
        alert('Saved successfully!');
    }

    $scope.previewProject = function() {
        var previewUrl = 'preview.html?config=' + projectService.getConfigName();
        window.open(previewUrl, '_blank');
    }

    var openHotspotPropertyPanel = function() {
        // $('#firstLevelPanel').addClass("in");
        $('#collapseExample').removeClass("in");
        $('#collapseExample3').removeClass("in");
        $('#collapseExample2').addClass("in");
    }

    var closeHotspotPropertyPanel = function() {
        // $('#firstLevelPanel').removeClass("in");
        $('#collapseExample2').removeClass("in");
        $('#collapseExample3').addClass("in");
    }

    var closeAddElementPanel = function() {
        // $('#firstLevelPanel').removeClass("in");
        $('#collapseExample').removeClass("in");
        $('#collapseExample3').addClass("in");
    }

    var openSceneModal = function() {
        $('#myModal').addClass("in");
        $('#myModal').css("display", "block");
    }

    var closeSceneModal = function() {
        $('#myModal').removeClass("in");
        $('#myModal').css("display", "none");
        $scope.selectedScene = null;
    }

    var openBgModal = function() {
        $('#changeBgModal').addClass("in");
        $('#changeBgModal').css("display", "block");
    }

    var closeBgModal = function() {
        $('#changeBgModal').removeClass("in");
        $('#changeBgModal').css("display", "none");
    }

    var changeSky = function() {
        document.querySelector('#environment-sky').setAttribute('src', $scope.sceneData.src);
    }

    var getCameraRotation = function() {
        var rotationValue = document.getElementById("camera").getAttribute("rotation");
        var rotation = rotationValue.x + " " + rotationValue.y + " " + rotationValue.z;
        return rotation;
    }

    var loadStitchedImages = function() {
        projectService.fetchStitchedImages(function(data) {
            $scope.stitchedImgsThumbs = data.map(function(e) {
                return appConfig.imgThumbServer + e;
            });
        });
    }

    var generateCenterPosition = function() {
        let rc = new THREE.Raycaster();
        let dist = 8.92;
        let cameraObj = document.querySelector('#camera').getObject3D('camera');
        let mouse = { x: 0, y: 0 };
        rc.setFromCamera(mouse, cameraObj);
        let point = rc.ray.at(dist);
        return point;
    }

    var init = function() {
        var configToLoad = $location.search().config;
        projectService.fetchData(configToLoad, function(data) {
            $scope.sceneData = projectService.getHomeSceneData();
            changeSky();
            $scope.scenesList = projectService.getScenesList();
            $scope.bgImages = projectService.getBgImages();
        });
        loadStitchedImages();
    };

    init();

});