app.constant('appConfig', {
    'backend': 'http://127.0.0.1:8000',
    'imgServer': 'http://127.0.0.1:8000/storage/img/result/',
    'imgThumbServer': 'http://127.0.0.1:8000/storage/img/previews/'
});
