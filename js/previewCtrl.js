app.controller("previewCtrl", function($scope, $rootScope, $location, projectService) {

    $scope.openScene = function(sceneName) {
        $scope.sceneData = projectService.getSceneData(sceneName);
        changeSky();
    };

    var init = function() {
        var configToLoad = $location.search().config;
        projectService.fetchData(configToLoad, function(data) {
            $scope.sceneData = projectService.getHomeSceneData();
            changeSky();
            $scope.scenesList = projectService.getScenesList();
        });
    };

    var changeSky = function(scene) {
        document.querySelector('#environment-sky').setAttribute('src', $scope.sceneData.src);
    }

    init();

});
