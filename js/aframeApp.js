var app = angular.module("aframeApp", ['ngFileUpload']);

app.config(function($locationProvider) {
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
});
