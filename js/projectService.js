app.service('projectService', function($http, appConfig) {
    var projectData = null;
    var config = "default"

    this.getProjectData = function(index) {
        return projectData;
    }

    this.setProjectData = function(data) {
        this.projectData = data;
    }

    this.addHotpotToScene = function(sceneName, hotspotData) {
        projectData.scenes.forEach(function(scene) {
            if (scene.name == sceneName) {
                scene.hotspots.push(hotspotData);
            }
        });
    }

    this.setHotspotTarget = function(currentSceneName, hotspotName, targetSceneName) {
        projectData.scenes.forEach(function(scene) {
            if (scene.name == currentSceneName) {
                scene.hotspots.forEach(function(hotspot) {
                    if (hotspot.name == hotspotName) {
                        hotspot.target = targetSceneName;
                    }
                });
            }
        });
    }

    this.getScenesList = function() {
        return projectData.scenes;
    }

    this.getSceneData = function(sceneName) {
        var sceneData = null;
        projectData.scenes.forEach(function(scene) {
            if (scene.name == sceneName) {
                sceneData = scene;
            }
        });
        return sceneData;
    }

    this.getHomeSceneData = function(data) {
        return projectData.scenes[0];
    }

    this.getBgImages = function() {
        return projectData.bgimages;
    }

    this.fetchData = function(configName, callback) {
        var dataUrl = appConfig.backend + "/vrproject"
        if (configName) {
            config = configName;
            dataUrl = dataUrl + "?config=" + configName;
        }
        $http.get(dataUrl).then(function mySucces(response) {
            projectData = response.data;
            callback(response.data);
        }, function myError(response) {
            alert('Please login again.');
            window.location = appConfig.backend + '/home';
        });
    }

    this.saveToServer = function() {
        $http.post(appConfig.backend + "/vrproject", { 'config': config, 'data': projectData }, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
            .then(function(response, status) {
                console.log('save successfully.');
            }, function(err) {
                console.log(err);
            });
    }

    this.fetchStitchedImages = function(callback) {
        var requestUrl = appConfig.backend + "/vrproject/stitchedImages";
        $http.get(requestUrl).then(function mySucces(response) {
            callback(response.data);
        }, function myError(response) {
            alert('Something is wrong, please login again.');
            window.location = appConfig.backend + '/home';
        });
    }

    this.generateRandomString = function() {
        return 'spot' + Math.random().toString(36).substring(7);
    }

    this.getConfigName = function() {
        return config;
    }
});
