<?php
$file = "./userdata/";
if (isset($_GET['config'])) {
    $configName = $_GET['config'];
    $file       = $file . $configName . ".json";
} else {
    $file .= "default.json";
}

$json = json_decode(file_get_contents($file), true);
echo json_encode($json);
