<?php
require 'vendor/autoload.php';

// Using Medoo namespace
use Medoo\Medoo;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'aframegui',
    'server'        => '127.0.0.1',
    'username'      => 'root',
    'password'      => 'root',
]);

// Enjoy
$database->insert('users', [
    'username' => 'test',
    'password' => 'test',
]);

$data = $database->select('users', [
    'username',
    'password',
]);

echo json_encode($data);
