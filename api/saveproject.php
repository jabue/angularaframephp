<?php
$file = "./userdata/";
if (isset($_GET['config'])) {
    $configName = $_GET['config'];
    $file       = $file . $configName . ".json";
} else {
    $file .= "default.json";
}
$postdata = file_get_contents("php://input");
$config   = json_decode($postdata);
file_put_contents($file, json_encode($config));
echo json_encode($config);
