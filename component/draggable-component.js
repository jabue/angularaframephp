AFRAME.registerComponent('draggable-component', {
    schema: {
        default: true
    },
    init: function() {
        var el = this.el;
        var property = this;
        var sceneEl = document.querySelector('a-scene');
        var cameraEl = document.querySelector('a-camera');
        var cursorEl = document.querySelector('a-cursor');
        var elId = null;
        var sphereEl = null;
        var sphereObj = null;
        var mouseDownFlag = false;
        var mouse = { x: 0, y: 0 };
        var cameraObj = this.el.getObject3D('camera');
        var dist = null;
        var mouseOnEntityFlag = false;

        cameraEl.addEventListener('mousedown', function(evt) {
            if(evt.detail.target.getAttribute('id') == document.querySelector('#currentHotspot').getAttribute('data')) {
                mouseOnEntityFlag = true;
            } else {
                mouseOnEntityFlag = false;
            }
        });

        sceneEl.addEventListener('mousedown', function() {
            mouseDownFlag = true;
            if (property.data) {
                elId = '#' + document.querySelector('#currentHotspot').getAttribute('data');
                sphereEl = document.querySelector(elId);
                sphereObj = sphereEl.object3D;
                if (!dist) {
                    dist = sphereObj.position.distanceTo(cameraObj.position);
                }
            }
        });

        sceneEl.addEventListener('mouseup', function() {
            mouseDownFlag = false;
        });

        sceneEl.addEventListener('mousemove', function(mouseData) {
            if (property.data && mouseOnEntityFlag) {
                if (mouseDownFlag) {
                    let rc = new THREE.Raycaster();
                    mouse.x = (mouseData.clientX / window.innerWidth) * 2 - 1;
                    mouse.y = -(mouseData.clientY / window.innerHeight) * 2 + 1;
                    rc.setFromCamera(mouse, cameraObj);
                    let point = rc.ray.at(dist);
                    sphereEl.setAttribute('position', point);
                }
            }
        });
    },
    update: function() {

    },
    updateSchema: function(data) {
        // console.log(data);
        // console.log(this.data);
    }
});